## 0.6.0
* Make `MalformedNameEncounteredException` a `RuntimeException` to indicate it is an unchecked exception and don't use AssertionError as it's meant for actual assertions

## 0.5.0
* Bump to `serenata/common` 0.3

## 0.4.1
* Fix incorrect `serenata/common` version

## 0.4.0
* Rename namespaces to Serenata

## 0.3.1
* Update deprecated license identifier

## 0.3.0
* Replace `LogicException`s with `AssertionError`s
* Replace `LogicException` in `PositionalNamespaceDeterminer` with catchable `PositionOutOfBoundsPositionalNamespaceDeterminerException`, which is also now present in the `PositionalNamespaceDeterminerInterface` interface via its base exception `PositionalNamespaceDeterminerException`

## 0.2.0
* Fix classlike imports not being used when resolving qualified function and constants:

```php
namespace A;

use B\C;

C\foo(); // Here `C` is actually `\B\C` as the import is used.
```

* Fix unqualified functions and constants being resolved to the root namespace, even when the root namespace had no such constant
  * The correct behavior is to check the active namespace, check the root namespace, and finally use the active namespace again if none of these exist. Previously, the root namespace variant was returned, which is incorrect.
* Fix qualified functions and constants being resolved against the root namespace when they were not found in the active namespace
  * Qualified functions and constants are never resolved to the root namespace. This only happens for unqualified ones, probably for backwards compatibility when namespaces were introduced in PHP.

## 0.1.0
* Initial release.
