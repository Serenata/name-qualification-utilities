<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Position;
use Serenata\Common\FilePosition;

use Serenata\NameQualificationUtilities\NameResolverInterface;
use Serenata\NameQualificationUtilities\StructureAwareNameResolver;
use Serenata\NameQualificationUtilities\FunctionPresenceIndicatorInterface;
use Serenata\NameQualificationUtilities\ConstantPresenceIndicatorInterface;
use Serenata\NameQualificationUtilities\PositionalNameResolverFactoryInterface;
use Serenata\NameQualificationUtilities\PositionalNamespaceDeterminerInterface;

use PhpSpec\ObjectBehavior;

class StructureAwareNameResolverFactorySpec extends ObjectBehavior
{
    public function it_creates_a_new_instance(
        PositionalNameResolverFactoryInterface $positionalNameResolverFactory,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator,
        NameResolverInterface $nameResolver
    ): void {
        $this->beConstructedWith(
            $positionalNameResolverFactory,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $position = new Position(1, 0);

        $filePosition = new FilePosition('test', $position);

        $positionalNameResolverFactory->create($filePosition)->willReturn($nameResolver);

        $this->create($filePosition)->shouldReturnAnInstanceOf(StructureAwareNameResolver::class);
    }
}
