<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Range;
use Serenata\Common\Position;

use Serenata\NameQualificationUtilities\Import;
use Serenata\NameQualificationUtilities\NameKind;
use Serenata\NameQualificationUtilities\Namespace_;

use Serenata\NameQualificationUtilities\Exception\MalformedNameEncounteredException;
use Serenata\NameQualificationUtilities\Exception\UnresolvableNameEncounteredException;

use PhpSpec\ObjectBehavior;

class NamespaceNameResolverSpec extends ObjectBehavior
{
    /**
     * @var Range
     */
    private $dummyRange;

    /**
     * @var Position
     */
    private $dummyPosition;

    /**
     * @return void
     */
    public function let()
    {
        $this->dummyRange = new Range(
            new Position(0, 0),
            new Position(1, 0)
        );

        $this->dummyPosition = new Position(0, 1);
    }

    /**
     * @return void
     */
    public function it_ignores_fully_qualified_names(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->resolve('\A\B')->shouldBe('\A\B');
    }

    /**
     * @return void
     */
    public function it_resolves_unqualified_classlike_names_to_root_namespace_when_not_in_namespace(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->resolve('A')->shouldBe('\A');
    }

    /**
     * @return void
     */
    public function it_uses_namespace_to_resolve_unqualified_classlike_names(): void
    {
        $this->beConstructedWith(new Namespace_('A', [], $this->dummyRange));

        $this->resolve('B')->shouldBe('\A\B');
    }

    /**
     * @return void
     */
    public function it_uses_imports_to_resolve_unqualified_classlike_names(): void
    {
        $items = [
            new Import('B\C\D', 'D', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('D')->shouldBe('\B\C\D');
    }

    /**
     * @return void
     */
    public function it_uses_imports_to_resolve_qualified_classlike_names(): void
    {
        $items = [
            new Import('B\C\D', 'D', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('D\E')->shouldBe('\B\C\D\E');
    }

    /**
     * @return void
     */
    public function it_uses_aliased_imports_to_resolve_unqualified_classlike_names(): void
    {
        $items = [
            new Import('B\C', 'Alias', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('Alias')->shouldBe('\B\C');
    }

    /**
     * @return void
     */
    public function it_uses_aliased_imports_to_resolve_qualified_classlike_names(): void
    {
        $items = [
            new Import('B\C', 'Alias', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('Alias\E')->shouldBe('\B\C\E');
    }

    /**
     * @return void
     */
    public function it_uses_function_imports_to_resolve_unqualified_function_names(): void
    {
        $items = [
            new Import('B\C\a_function', 'a_function', NameKind::FUNCTION_, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('a_function', NameKind::FUNCTION_)->shouldBe('\B\C\a_function');
    }

    /**
     * @return void
     */
    public function it_uses_class_imports_to_resolve_qualified_function_names(): void
    {
        $items = [
            new Import('B\C', 'D', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('D\a_function', NameKind::FUNCTION_)->shouldBe('\B\C\a_function');
    }

    /**
     * @return void
     */
    public function it_uses_constant_imports_to_resolve_unqualified_constant_names(): void
    {
        $items = [
            new Import('B\C\a_constant', 'a_constant', NameKind::CONSTANT, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('a_constant', NameKind::CONSTANT)->shouldBe('\B\C\a_constant');
    }

    /**
     * @return void
     */
    public function it_uses_class_imports_to_resolve_qualified_constant_names(): void
    {
        $items = [
            new Import('B\C', 'D', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('D\a_constant', NameKind::CONSTANT)->shouldBe('\B\C\a_constant');
    }

    /**
     * @return void
     */
    public function it_filters_imports_by_kind_during_resolving(): void
    {
        $items = [
            new Import('A\name', 'name', NameKind::CLASSLIKE, $this->dummyPosition),
            new Import('B\name', 'name', NameKind::FUNCTION_, $this->dummyPosition),
            new Import('C\name', 'name', NameKind::CONSTANT, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->resolve('name', NameKind::CLASSLIKE)->shouldBe('\A\name');
        $this->resolve('name', NameKind::FUNCTION_)->shouldBe('\B\name');
        $this->resolve('name', NameKind::CONSTANT)->shouldBe('\C\name');
    }

    /**
     * @return void
     */
    public function it_does_not_fail_on_zero(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->resolve('0')->shouldBe('\0');
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_empty_names(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(MalformedNameEncounteredException::class)->duringResolve('');
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_unqualified_constants(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(UnresolvableNameEncounteredException::class)->duringResolve('SOME_CONSTANT', NameKind::CONSTANT);
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_qualified_constants(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(UnresolvableNameEncounteredException::class)->duringResolve('A\SOME_CONSTANT', NameKind::CONSTANT);
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_unqualified_functions(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(UnresolvableNameEncounteredException::class)->duringResolve('some_function', NameKind::FUNCTION_);
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_qualified_functions(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(UnresolvableNameEncounteredException::class)->duringResolve('A\some_function', NameKind::FUNCTION_);
    }
}
