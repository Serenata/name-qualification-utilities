<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Range;
use Serenata\Common\Position;
use Serenata\Common\FilePosition;

use Serenata\NameQualificationUtilities\NameKind;
use Serenata\NameQualificationUtilities\Exception;
use Serenata\NameQualificationUtilities\Namespace_;
use Serenata\NameQualificationUtilities\NameResolverInterface;
use Serenata\NameQualificationUtilities\FunctionPresenceIndicatorInterface;
use Serenata\NameQualificationUtilities\ConstantPresenceIndicatorInterface;
use Serenata\NameQualificationUtilities\PositionalNamespaceDeterminerInterface;

use PhpSpec\ObjectBehavior;

class StructureAwareNameResolverSpec extends ObjectBehavior
{
    /**
     * @var Range
     */
    private $dummyRange;

    /**
     * @var Position
     */
    private $dummyPosition;

    /**
     * @return void
     */
    public function let()
    {
        $this->dummyRange = new Range(
            new Position(0, 0),
            new Position(1, 0)
        );

        $this->dummyPosition = new Position(0, 1);
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_passes_through_when_delegate_returns_valid_result(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $result = '\Test';

        $delegate->resolve('Test', NameKind::CLASSLIKE)->willReturn($result);

        $this->resolve('Test', new FilePosition('Test.php', new Position(0, 0)), NameKind::CLASSLIKE)->shouldBe($result);
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_tries_unqualified_constant_relative_to_active_namespace(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        // TODO

        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('Test', NameKind::CONSTANT)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $constantPresenceIndicator->isPresent('\N\Test')->willReturn(true);

        $this->resolve('Test', new FilePosition($fileName, $position), NameKind::CONSTANT)->shouldBe('\N\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_tries_unqualified_constant_in_root_namespace_if_constant_relative_to_active_namespace_does_not_exist(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        // TODO

        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $filePosition = new FilePosition('Test.php', new Position(1, 0));

        $delegate->resolve('Test', NameKind::CONSTANT)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $constantPresenceIndicator->isPresent('\N\Test')->willReturn(false);
        $constantPresenceIndicator->isPresent('\Test')->willReturn(true);

        $this->resolve('Test', $filePosition, NameKind::CONSTANT)->shouldBe('\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_does_not_try_qualified_constant_relative_to_root_namespace_if_constant_relative_to_active_namespace_does_not_exist(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('B\Test', NameKind::CONSTANT)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $constantPresenceIndicator->isPresent('\N\B\Test')->willReturn(false);
        $constantPresenceIndicator->isPresent('\B\Test')->willReturn(true);

        $this->resolve('B\Test', new FilePosition($fileName, $position), NameKind::CONSTANT)->shouldBe('\N\B\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_makes_unqualified_constant_relative_to_active_namespace_if_it_exists_in_neither_active_namespace_nor_root_namespace(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $filePosition = new FilePosition('Test.php', new Position(1, 0));

        $delegate->resolve('Test', NameKind::CONSTANT)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $constantPresenceIndicator->isPresent('\N\Test')->willReturn(false);
        $constantPresenceIndicator->isPresent('\Test')->willReturn(false);

        $this->resolve('Test', $filePosition, NameKind::CONSTANT)->shouldBe('\N\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_tries_unqualified_function_relative_to_active_namespace(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('Test', NameKind::FUNCTION_)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $functionPresenceIndicator->isPresent('\N\Test')->willReturn(true);

        $this->resolve('Test', new FilePosition($fileName, $position), NameKind::FUNCTION_)->shouldBe('\N\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_tries_unqualified_function_relative_to_root_namespace_if_function_relative_to_active_namespace_does_not_exist(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('Test', NameKind::FUNCTION_)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $functionPresenceIndicator->isPresent('\N\Test')->willReturn(false);
        $functionPresenceIndicator->isPresent('\Test')->willReturn(true);

        $this->resolve('Test', new FilePosition($fileName, $position), NameKind::FUNCTION_)->shouldBe('\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_does_not_try_qualified_function_relative_to_root_namespace_if_function_relative_to_active_namespace_does_not_exist(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('B\Test', NameKind::FUNCTION_)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $functionPresenceIndicator->isPresent('\N\B\Test')->willReturn(false);
        $functionPresenceIndicator->isPresent('\B\Test')->willReturn(true);

        $this->resolve('B\Test', new FilePosition($fileName, $position), NameKind::FUNCTION_)->shouldBe('\N\B\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_makes_unqualified_function_relative_to_active_namespace_if_it_exists_in_neither_active_namespace_nor_root_namespace(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        // TODO

        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $filePosition = new FilePosition($fileName, $position);

        $delegate->resolve('Test', NameKind::FUNCTION_)->willThrow(new Exception\UnresolvableNameEncounteredException());
        $positionalNamespaceDeterminer->determine($filePosition)->willReturn(new Namespace_(
            'N',
            [],
            new Range(new Position(0, 0), new Position(10, 0))
        ));
        $functionPresenceIndicator->isPresent('\N\Test')->willReturn(false);
        $functionPresenceIndicator->isPresent('\Test')->willReturn(false);

        $this->resolve('Test', new FilePosition($fileName, $position), NameKind::FUNCTION_)->shouldBe('\N\Test');
    }

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     *
     * @return void
     */
    public function it_rethrows_when_it_does_not_know_how_to_handle_kind(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ): void {
        $this->beConstructedWith(
            $delegate,
            $positionalNamespaceDeterminer,
            $constantPresenceIndicator,
            $functionPresenceIndicator
        );

        $fileName = 'Test.php';
        $position = new Position(1, 0);
        $exception = new Exception\UnresolvableNameEncounteredException();

        $delegate->resolve('Test', NameKind::CLASSLIKE)->willThrow($exception);

        $this->shouldThrow($exception)->duringResolve('Test', new FilePosition($fileName, $position), NameKind::CLASSLIKE);
    }
}
