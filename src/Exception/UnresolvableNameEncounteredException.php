<?php

namespace Serenata\NameQualificationUtilities\Exception;

use RuntimeException;

/**
 * Exception that indicates resolving a name is immpossible because there is not enough information.
 */
class UnresolvableNameEncounteredException extends RuntimeException
{
}
