<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Indicates the requested position was out of bounds.
 */
final class PositionOutOfBoundsPositionalNamespaceDeterminerException extends PositionalNamespaceDeterminerException
{

}
