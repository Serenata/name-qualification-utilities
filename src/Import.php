<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\Position;

/**
 * Represents data for a namespace.
 *
 * This is a value object and immutable.
 */
final class Import
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $kind;

    /**
     * @var Position
     */
    private $appliesAfter;

    /**
     * @param string   $name         The name that was imported, e.g. "A\B", "B\someFunc", ...
     * @param string   $alias        The alias the name was aliased to, "use A\B" becomes "B", "use A\B as C" becomes "C", ...
     * @param string   $kind         Kind of name, a constant from {@see NameKind}.
     * @param Position $appliesAfter The position where the import starts applying from (usually just after its semi-colon).
     *
     * @throws Exception\MalformedNameEncounteredException
     */
    public function __construct(string $name, string $alias, string $kind, Position $appliesAfter)
    {
        if ($name === '') {
            throw new Exception\MalformedNameEncounteredException('Name of import can\'t be an empty string');
        } elseif ($alias === '') {
            throw new Exception\MalformedNameEncounteredException('Alias of import can\'t be an empty string');
        } elseif ($kind === '') {
            throw new Exception\MalformedNameEncounteredException('Kind of import can\'t be an empty string');
        }

        $this->name = $name;
        $this->alias = $alias;
        $this->kind = $kind;
        $this->appliesAfter = $appliesAfter;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @return Position
     */
    public function getAppliesAfter(): Position
    {
        return $this->appliesAfter;
    }

    /**
     * @return string
     */
    public function getFullyQualifiedName(): string
    {
        if ($this->getName()[0] !== '\\') {
            return '\\' . $this->getName();
        }

        return $this->getName();
    }
}
