<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Creates instances of {@see NameResolverInterface} that can resolve names at a specific position in a file.
 */
final class PositionalNameResolverFactory implements PositionalNameResolverFactoryInterface
{
    /**
     * @var PositionalNamespaceDeterminerInterface
     */
    private $positionalNamespaceDeterminerInterface;

    /**
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminerInterface
     */
    public function __construct(PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminerInterface)
    {
        $this->positionalNamespaceDeterminerInterface = $positionalNamespaceDeterminerInterface;
    }

    /**
     * @inheritDoc
     */
    public function create(FilePosition $filePosition): NameResolverInterface
    {
        return new NamespaceNameResolver(
            $this->positionalNamespaceDeterminerInterface->determine($filePosition)
        );
    }
}
