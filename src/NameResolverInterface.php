<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Interface for classes that can resolve local names to their fully qualified name.
 */
interface NameResolverInterface
{
    /**
     * @param string $name The name to resolve.
     * @param string $kind Kind of type to resolve. A constant from {@see NameKind}.
     *
     * @throws Exception\MalformedNameEncounteredException
     * @throws Exception\UnresolvableNameEncounteredException
     *
     * @return string
     */
    public function resolve(string $name, string $kind = NameKind::CLASSLIKE): string;
}
