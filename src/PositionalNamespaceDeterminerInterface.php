<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Interface for classes that determine which {@see Namespace_} is active at a specific location in a file.
 */
interface PositionalNamespaceDeterminerInterface
{
    /**
     * Returns the namespace that is active at the specified file and location.
     *
     * The returned namespace's imports are limited to those that apply to the specified position as well. Imports that
     * are only relevant after the location are not included.
     *
     * @param FilePosition $filePosition
     *
     * @throws PositionalNamespaceDeterminerException
     *
     * @return Namespace_
     */
    public function determine(FilePosition $filePosition): Namespace_;
}
