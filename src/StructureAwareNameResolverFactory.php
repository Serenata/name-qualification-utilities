<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Factory that creates instances of a {@see StructureAwareNameResolver}.
 */
final class StructureAwareNameResolverFactory implements StructureAwareNameResolverFactoryInterface
{
    /**
     * @var PositionalNameResolverFactoryInterface
     */
    private $positionalNameResolverFactory;

    /**
     * @var PositionalNamespaceDeterminerInterface
     */
    private $positionalNamespaceDeterminer;

    /**
     * @var ConstantPresenceIndicatorInterface
     */
    private $constantPresenceIndicator;

    /**
     * @var FunctionPresenceIndicatorInterface
     */
    private $functionPresenceIndicator;

    /**
     * @param PositionalNameResolverFactoryInterface $positionalNameResolverFactory
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     */
    public function __construct(
        PositionalNameResolverFactoryInterface $positionalNameResolverFactory,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ) {
        $this->positionalNameResolverFactory = $positionalNameResolverFactory;
        $this->positionalNamespaceDeterminer = $positionalNamespaceDeterminer;
        $this->constantPresenceIndicator = $constantPresenceIndicator;
        $this->functionPresenceIndicator = $functionPresenceIndicator;
    }

    /**
     * @inheritDoc
     */
    public function create(FilePosition $filePosition): PositionalNameResolverInterface
    {
        $nameResolver = $this->positionalNameResolverFactory->create($filePosition);

        return new StructureAwareNameResolver(
            $nameResolver,
            $this->positionalNamespaceDeterminer,
            $this->constantPresenceIndicator,
            $this->functionPresenceIndicator
        );
    }
}
