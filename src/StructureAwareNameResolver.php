<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Resolves local names to their FQCN, taking structural data into account.
 *
 * Some names can not be resolved without knowledge about the structural elements inside a project or code base. This
 * resolver takes into account this information and is able to resolve types in situations where they are ambiguous
 * without this information (and where {@see NamespaceNameResolver} would throw an exception).
 */
final class StructureAwareNameResolver implements PositionalNameResolverInterface
{
    /**
     * @var NameResolverInterface
     */
    private $delegate;

    /**
     * @var PositionalNamespaceDeterminerInterface
     */
    private $positionalNamespaceDeterminer;

    /**
     * @var ConstantPresenceIndicatorInterface
     */
    private $constantPresenceIndicator;

    /**
     * @var FunctionPresenceIndicatorInterface
     */
    private $functionPresenceIndicator;

    /**
     * @param NameResolverInterface                  $delegate
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     * @param ConstantPresenceIndicatorInterface     $constantPresenceIndicator
     * @param FunctionPresenceIndicatorInterface     $functionPresenceIndicator
     */
    public function __construct(
        NameResolverInterface $delegate,
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer,
        ConstantPresenceIndicatorInterface $constantPresenceIndicator,
        FunctionPresenceIndicatorInterface $functionPresenceIndicator
    ) {
        $this->delegate = $delegate;
        $this->positionalNamespaceDeterminer = $positionalNamespaceDeterminer;
        $this->constantPresenceIndicator = $constantPresenceIndicator;
        $this->functionPresenceIndicator = $functionPresenceIndicator;
    }

    /**
     * @inheritDoc
     */
    public function resolve(string $name, FilePosition $filePosition, string $kind = NameKind::CLASSLIKE): string
    {
        try {
            return $this->delegate->resolve($name, $kind);
        } catch (Exception\UnresolvableNameEncounteredException $e) {
            if ($kind === NameKind::CONSTANT) {
                return $this->resolveConstant($name, $filePosition);
            } elseif ($kind === NameKind::FUNCTION_) {
                return $this->resolveFunction($name, $filePosition);
            }

            throw $e;
        }
    }

    /**
     * @param string       $name
     * @param FilePosition $filePosition
     *
     * @return string
     */
    protected function resolveConstant(string $name, FilePosition $filePosition): string
    {
        $namespace = $this->positionalNamespaceDeterminer->determine($filePosition);

        if ($namespace->getName() === null) {
            return '\\' . $name;
        }

        // See if it exists in active namespace. If so, use that.
        $namespacedName = '\\' . $namespace->getName() . '\\' . $name;

        if ($this->constantPresenceIndicator->isPresent($namespacedName)) {
            return $namespacedName;
        }

        if (count(explode('\\', $name)) === 1) {
            // See if it exists in root namespace. If so, use that. This only happens for unqualified constants,
            // probably for backwards compatibility when namespaces were first introduced.
            $nameAsFullyQualifiedName = '\\' . $name;

            if ($this->constantPresenceIndicator->isPresent($nameAsFullyQualifiedName)) {
                return $nameAsFullyQualifiedName;
            }
        }

        // If neither active namespace nor root namespace variant exist, fall back to active namespace variant.
        return $namespacedName;
    }

    /**
     * @param string       $name
     * @param FilePosition $filePosition
     *
     * @return string
     */
    protected function resolveFunction(string $name, FilePosition $filePosition): string
    {
        $namespace = $this->positionalNamespaceDeterminer->determine($filePosition);

        if ($namespace->getName() === null) {
            return '\\' . $name;
        }

        // See if it exists in active namespace. If so, use that.
        $namespacedName = '\\' . $namespace->getName() . '\\' . $name;

        if ($this->functionPresenceIndicator->isPresent($namespacedName)) {
            return $namespacedName;
        }

        if (count(explode('\\', $name)) === 1) {
            // See if it exists in root namespace. If so, use that. This only happens for unqualified functions,
            // probably for backwards compatibility when namespaces were first introduced.
            $nameAsFullyQualifiedName = '\\' . $name;

            if ($this->functionPresenceIndicator->isPresent($nameAsFullyQualifiedName)) {
                return $nameAsFullyQualifiedName;
            }
        }

        // If neither active namespace nor root namespace variant exist, fall back to active namespace variant.
        return $namespacedName;
    }
}
