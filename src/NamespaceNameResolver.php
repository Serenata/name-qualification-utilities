<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Resolves local names to their FQCN.
 */
final class NamespaceNameResolver implements NameResolverInterface
{
    /**
     * @var Namespace_
     */
    private $namespace;

    /**
     * @param Namespace_ $namespace
     */
    public function __construct(Namespace_ $namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @inheritDoc
     */
    public function resolve(string $name, string $kind = NameKind::CLASSLIKE): string
    {
        if ($name === '') {
            throw new Exception\MalformedNameEncounteredException('Name to resolve can\'t be empty');
        } elseif ($name[0] === '\\') {
            return $name;
        }

        $parts = explode('\\', $name);

        if (count($parts) === 1) {
            $fullName = $this->findFullyQualifiedNameViaImports($name, $kind);
        } else {
            // For qualified function names, such as A\foo, we need to examine the namespace (e.g. A) instead.
            $fullName = $this->findFullyQualifiedNameViaImports($name, NameKind::CLASSLIKE);
        }

        if ($fullName !== null) {
            return $fullName;
        } elseif ($kind !== NameKind::CLASSLIKE) {
            // Unqualified constant or functions names could be relative to the current namespace OR be part of the
            // root namespace. Which one of the two is used by PHP depends on which one exists, which is not known
            // to us.
            throw new Exception\UnresolvableNameEncounteredException(
                'Unqualified name can\'t be resolved without further information'
            );
        }

        // Still here? There must be no explicit use statement, default to the current namespace.
        return ($this->namespace->getFullyQualifiedName() ?: '') . '\\' . $name;
    }

    /**
     * @param string $name
     * @param string $kind
     *
     * @return string|null
     */
    protected function findFullyQualifiedNameViaImports(string $name, string $kind = NameKind::CLASSLIKE): ?string
    {
        $nameParts = explode('\\', $name);
        $firstNamePart = array_shift($nameParts);

        $import = $this->namespace->findImportForAlias($firstNamePart, $kind);

        if (!$import) {
            return null;
        }

        if (empty($nameParts)) {
            return $import->getFullyQualifiedName();
        }

        return $import->getFullyQualifiedName() . '\\' . implode('\\', $nameParts);
    }
}
