<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Interface for classes that can provide a list of {@see Namespace_} objects for a specific file.
 */
interface FileNamespaceProviderInterface
{
    /**
     * @param string $file
     *
     * @return Namespace_[]
     */
    public function provide(string $file): array;
}
